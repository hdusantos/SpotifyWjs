import chai, { expect } from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import SpotifyWjs from '../src/index';

chai.use(sinonChai);
global.fetch = require('node-fetch');

describe('SpotifyWjs', () => {
  it('Should create an instance of SpotifyWjs', () => {
    const spotify = new SpotifyWjs({});
    expect(spotify).to.be.instanceof(SpotifyWjs);
  });
  it('Should receive apiURL as an option', () => {
    const spotify = new SpotifyWjs({
      apiURL: 'aaa.bbb',
    });
    expect(spotify.apiURL).to.be.equal('aaa.bbb');
  });
  it('Should use default apiURL if not provided', () => {
    const spotify = new SpotifyWjs({});
    expect(spotify.apiURL).to.be.equal('https://api.spotify.com/v1');
  });
  it('Should receive token as an option', () => {
    const spotify = new SpotifyWjs({
      token: 'abc',
    });
    expect(spotify.token).to.be.equal('abc');
  });

  describe('Request method', () => {
    let stubedFetch;
    let promise;

    beforeEach(() => {
      stubedFetch = sinon.stub(global, 'fetch');
      promise = stubedFetch.resolves({ json: () => ({ body: 'json' }) });
    });
    afterEach(() => {
      stubedFetch.restore();
    });

    it('Should exist the request method', () => {
      const spotify = new SpotifyWjs({});
      expect(spotify.request).to.be.a('function');
    });
    it('Should call fetch function', () => {
      const spotify = new SpotifyWjs({});
      spotify.request();
      expect(stubedFetch).to.have.been.calledOnce;
    });
    it('Should receive the correct url to fetch', () => {
      const spotify = new SpotifyWjs({
        token: 'tokenSpotify',
      });
      spotify.request('aaa.abc');
      expect(stubedFetch).to.have.been.calledWith('aaa.abc');
    });
    it('Should call with right headers passed', () => {
      const spotify = new SpotifyWjs({
        token: 'tokenSpotify',
      });
      const headers = {
        headers: {
          Authorization: 'Bearer tokenSpotify',
        },
      };
      spotify.request('aaa.abc', headers);
      expect(stubedFetch).to.have.been.calledWith('aaa.abc', headers);
    });
  });
});

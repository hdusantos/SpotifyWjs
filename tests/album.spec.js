import chai, { expect } from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import SpotifyWjs from '../src/index';

chai.use(sinonChai);
global.fetch = require('node-fetch');

describe('Album', () => {
  let stubedFetch;
  let promise;
  let spotify;

  beforeEach(() => {
    spotify = new SpotifyWjs({
      token: 'tokenSpotify',
    });
    stubedFetch = sinon.stub(global, 'fetch');
    promise = stubedFetch.resolves({ json: () => ({ body: 'json' }) });
  });
  afterEach(() => {
    stubedFetch.restore();
  });

  describe('Smoke test', () => {
    it('Should exist the getAlbum method', () => {
      expect(spotify.album.getAlbum).to.be.a('function');
    });
    it('Should exist the getAlbums method', () => {
      expect(spotify.album.getAlbums).to.be.a('function');
    });
    it('Should exist the getTracks method', () => {
      expect(spotify.album.getTracks).to.be.a('function');
    });
  });

  describe('getAlbum', () => {
    it('Should call fetch function', () => {
      const album = spotify.album.getAlbum();
      expect(stubedFetch).to.have.been.calledOnce;
    });
    it('Should receive the correct url to fetch', () => {
      const album = spotify.album.getAlbum('2xRddGxRH0KdDWLNE5b0iu');
      expect(stubedFetch).to.have.been.calledWith('https://api.spotify.com/v1/albums/2xRddGxRH0KdDWLNE5b0iu');
    });
    it('Should return the JSON data from the promise', () => {
      const album = spotify.album.getAlbum('2xRddGxRH0KdDWLNE5b0iu');
      album.then((data) => {
        expect(data).to.be.eql({ body: 'json' });
      });
    });
  });

  describe('getAlbums', () => {
    it('Should call fetch function', () => {
      const album = spotify.album.getAlbums();
      expect(stubedFetch).to.have.been.calledOnce;
    });
    it('Should receive the correct url to fetch', () => {
      const album = spotify.album.getAlbums(['2xRddGxRH0KdDWLNE5b0iu', '4EGy60w4omhOI8AJRxCSzI']);
      expect(stubedFetch).to.have.been.calledWith('https://api.spotify.com/v1/albums?ids=2xRddGxRH0KdDWLNE5b0iu%2C4EGy60w4omhOI8AJRxCSzI');
    });
    it('Should return the JSON data from the promise', () => {
      const album = spotify.album.getAlbums('2xRddGxRH0KdDWLNE5b0iu');
      album.then((data) => {
        expect(data).to.be.eql({ body: 'json' });
      });
    });
  });

  describe('getTracks', () => {
    it('Should call fetch function', () => {
      const album = spotify.album.getTracks();
      expect(stubedFetch).to.have.been.calledOnce;
    });
    it('Should receive the correct url to fetch', () => {
      const album = spotify.album.getTracks('2xRddGxRH0KdDWLNE5b0iu');
      expect(stubedFetch).to.have.been.calledWith('https://api.spotify.com/v1/albums/2xRddGxRH0KdDWLNE5b0iu/tracks');
    });
    it('Should return the JSON data from the promise', () => {
      const album = spotify.album.getTracks('2xRddGxRH0KdDWLNE5b0iu');
      album.then((data) => {
        expect(data).to.be.eql({ body: 'json' });
      });
    });
  });
});

import chai, { expect } from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

import SpotifyWjs from '../src/index';

chai.use(sinonChai);

global.fetch = require('node-fetch');

describe('Search', () => {
  let spotify;
  let stubedFetch;
  let promise;

  beforeEach(() => {
    spotify = new SpotifyWjs({
      token: 'tokenSpotify',
    });

    stubedFetch = sinon.stub(global, 'fetch');
    promise = stubedFetch.resolves({ json: () => ({ body: 'json' }) });
  });
  afterEach(() => {
    stubedFetch.restore();
  });

  describe('Smoke test', () => {
    it('Should exist the spotify.search.albums method', () => {
      expect(spotify.search.albums).to.be.a('function');
    });

    it('Should exist the spotify.search.artists method', () => {
      expect(spotify.search.artists).to.be.a('function');
    });

    it('Should exist the spotify.search.tracks method', () => {
      expect(spotify.search.tracks).to.be.a('function');
    });

    it('Should exist the spotify.search.playlists method', () => {
      expect(spotify.search.playlists).to.be.a('function');
    });
  });
  describe('spotify.search.artists', () => {
    it('Shoul call fetch function', () => {
      const artist = spotify.search.artists('legiao urbana');
      expect(stubedFetch).to.have.been.calledOnce;
    });
    it('Should receive the correct url to fetch', () => {
      const artist = spotify.search.artists('legiao urbana');
      expect(stubedFetch).to.have.been.calledWith('https://api.spotify.com/v1/search?q=legiao%20urbana&type=artist');
    });
  });
  describe('spotify.search.albums', () => {
    it('Shoul call fetch function', () => {
      const artist = spotify.search.albums('raul seixas');
      expect(stubedFetch).to.have.been.calledOnce;
    });
    it('Should receive the correct url to fetch', () => {
      const artist = spotify.search.albums('raul seixas');
      expect(stubedFetch).to.have.been.calledWith('https://api.spotify.com/v1/search?q=raul%20seixas&type=album');
    });
  });
  describe('spotify.search.tracks', () => {
    it('Shoul call fetch function', () => {
      const artist = spotify.search.tracks('raul seixas');
      expect(stubedFetch).to.have.been.calledOnce;
    });
    it('Should receive the correct url to fetch', () => {
      const artist = spotify.search.tracks('raul seixas');
      expect(stubedFetch).to.have.been.calledWith('https://api.spotify.com/v1/search?q=raul%20seixas&type=track');
    });
  });
  describe('spotify.search.playlists', () => {
    it('Shoul call fetch function', () => {
      const artist = spotify.search.playlists('raul seixas');
      expect(stubedFetch).to.have.been.calledOnce;
    });
    it('Should receive the correct url to fetch', () => {
      const artist = spotify.search.playlists('raul seixas');
      expect(stubedFetch).to.have.been.calledWith('https://api.spotify.com/v1/search?q=raul%20seixas&type=playlist');
    });
  });
});

export default function album() {
  return {
    getAlbum: id => this.request(`${this.apiURL}/albums/${id}`),
    getAlbums: ids => this.request(`${this.apiURL}/albums?ids=${encodeURIComponent(ids)}`),
    getTracks: id => this.request(`${this.apiURL}/albums/${encodeURIComponent(id)}/tracks`),
  };
}
